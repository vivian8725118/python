#!/usr/bin/env python3
#coding:utf-8
#todo 页数，keyword词库，识别二维码图片

import requests,re,json,time,traceback
from bs4 import BeautifulSoup

headers = {
	'User-Agent': 'Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1',
	'Cookie': 'gr_user_id=762db505-c649-47be-9270-d43c3a04404f; UM_distinctid=15ab6497b6d8e1-0ee0ea55dc05d1-1d3b6853-fa000-15ab6497b6e8b0; CNZZDATA1258679142=967056317-1471404233-https%253A%252F%252Fwww.google.co.jp%252F%7C1493210562; __utma=194070582.560583772.1461809437.1504250015.1504409331.810; __utmc=194070582; __utmz=194070582.1504409331.810.248.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=194070582.|2=User%20Type=Member=1; _gat=1; remember_user_token=W1sxOTY4OTRdLCIkMmEkMTAkOFp6LkZ4ZTJBYUNPSk5JS08vZkxYZSIsIjE1MDQ0MjMwMTcuMDg2NjI3Il0%3D--3b6782da1d011611f9db3a86fbf8b11e25f63254; _ga=GA1.2.560583772.1461809437; _gid=GA1.2.1465931911.1504409330; Hm_lvt_0c0e9d9b1e7d617b3e6842e85b9fb068=1504256133,1504256148,1504409331,1504414336; Hm_lpvt_0c0e9d9b1e7d617b3e6842e85b9fb068=1504423018; _maleskine_session=UURORFVHdnpQTFdiQ2JDeC9VcWI2THM0ay9xbVNtUWRnaVYzRFh6UElyV0N6RnhKN3UrU01RdUgreHp4MnBjMWNnaHhIaWh6a3pPbkZ1bldkd210UGZvTHFzcVlQLzR3MXp6RkUvWWgyUTczYThoblpkeXhvVHhUQ01KMCs1UkJlZmVhRWd0SSsrazdvM2lwV2d3V3JkYU9oY1RScitSN3V4MmxpcU00MXR3NFNjMVFTakxpUzJobDRtUFVoTTlWTG1ScVFrWEV4OXBWd2VyY2ZvRUsxTTZyVFM5UDFUV0h2NUtabG9kdmU0b3BKNStWV04ydWxKbzhyOUtjdERmMi0tUjhEdWRiM1lwWmVJNlZ5eiswL0pKUT09--ecc28d50aa72b08c1d9896d612e8778048c4d4bf',
	'Host': 'www.jianshu.com',
	'Connection': 'keep-alive',
	'Origin':'http://www.jianshu.com',
	'Accept': 'application/json'
}

programmer_subject_root_url = 'http://www.jianshu.com/notifications#/collections/NEt52a/submissions'
programmer_subject_page_url = 'http://www.jianshu.com/collections/NEt52a/collection_submissions?state=pending&page=%d'
android_subject_root_url = 'http://www.jianshu.com/notifications#/collections/0dc880a2c73c/submissions'
android_subject_page_url = 'http://www.jianshu.com/collections/0dc880a2c73c/collection_submissions?state=pending&page=%d'

def download_html(url):
	response = requests.get(url,headers=headers)
	if response.status_code != 200:
		print('response.status_code = ',response.status_code)
		return None
	return response.text

#黑名单
black_list = ['冷_6986','java大湿兄','代码技巧','煎饼铺','DuYang_ZXM','可惜没_如果','web前端程序员','菜鸟窝','樱美印刷小卢','小声讲故事','米国小米','mc毕加索']
black_list_len = len(black_list)

def is_in_black_list(username):
	if username in black_list:
		return True
	return False

#白名单
white_list = ['冷之秋','六尺帐篷','Donkor','jjs4500','不知霜舞哀伤udspj','程序亦非猿','valenhua的专栏']
white_list_len = len(white_list)

def is_in_white_list(username):
	if username in white_list:
		return True
	return False

#关键字
keywords = ['公众号','二维码','VPN','QQ群','群号','转自','QQ 群','公众号：','微信号','生产厂家','投资理财平台']
keywords_len = len(keywords)
keyword = ''

def has_key_word(slug):
	article_url = 'http://www.jianshu.com/p/%s' % slug
	print(article_url)
	article_html = requests.get(article_url).text
	soup = BeautifulSoup(article_html,'lxml')
	article_content = soup.find('div',class_='show-content')#只搜索文章内容里边的关键字
	if len(article_content.text)<250:#字数太少
		return True
	# print(article_content)
	for i in range(keywords_len):
		m = re.search(keywords[i],article_content.text)
		if m != None:
			print('search result:' + m.group())
			return True
	return False

def has_key_word_text(slug):
	article_url = 'http://www.jianshu.com/p/%s' % slug
	print(article_url)
	article_html = requests.get(article_url).text
	soup = BeautifulSoup(article_html,'lxml')
	article_content = soup.find('div',class_='show-content')#只搜索文章内容里边的关键字
	if len(article_content.text)<350:#字数太少
		return '内容太少'
	# print(article_content)
	for i in range(keywords_len):
		m = re.search(keywords[i],article_content.text)
		if m != None:
			print('search result:' + m.group())
			keyword = keywords[i]
			return keywords[i]
	keyword = ''
	return None

#推荐
recommend_list = ['YoKey','非著名程序员','程序员联盟','一缕殇流化隐半边冰霜','小鄧子','唐晓阳']
recommend_len = len(recommend_list)

def is_in_recommend_list(username):
	if username in recommend_list:
		return True
	return False

def recommend_article(note_id):
	recommend_url = 'http://www.jianshu.com/notes/%s/submit' % note_id
	payload = {'collection_id':'501921'}
	recommend_response = requests.post(recommend_url,headers=headers,json=payload)
	payload = {'collection_id':'47'}
	recommend_response = requests.post(recommend_url,headers=headers,json=payload)
	print('推荐文章')
	print(recommend_response.status_code)

def reject_article(id):
	reject_url = 'http://www.jianshu.com/collection_submissions/%s/reject' % id  
	payload = {'reject_reason':''}
	print(reject_url)
	reject_response = requests.put(reject_url,headers=headers,json=payload)
	print(reject_url)
	print(reject_response.status_code)

def reject_article_with_reason(id,reject_reason):
	reject_url = 'http://www.jianshu.com/collection_submissions/%s/reject' % id  
	payload = {'reject_reason':reject_reason}
	reject_response = requests.put(reject_url,headers=headers,json=payload)
	print(reject_url)
	print(reject_response.status_code)

def accept_article(id):
	accept_url = 'http://www.jianshu.com/collection_submissions/%s/approve' % id
	accept_response = requests.put(accept_url,headers=headers)
	print(accept_url)
	print(accept_response.status_code)

if __name__ == '__main__':
	while(True):
		# root_url = android_subject_root_url
		root_url = programmer_subject_root_url
		html_content = download_html(root_url)
		soup = BeautifulSoup(html_content,'lxml')
		print('正在解析，请稍后...')
		try:
			for i in range(1):
				# page_url = android_subject_page_url % i
				page_url = programmer_subject_page_url % i
				page_html_content = download_html(page_url)
				# print(page_html_content)
				collection_submissions=json.loads(page_html_content).get('collection_submissions')
				# print(collection_submissions)
				for collection_submission in collection_submissions:
					# print(collection_submission)
					note = collection_submission.get('note')
					user = note.get('user')
					username = user.get('nickname')
					id = collection_submission.get('id')
					note_id = note.get('id')
					slug = note.get('slug')
					print(username)
					if is_in_black_list(username):#黑名单
					   print('is_in_black_list')
					   reject_article(id)
					elif is_in_white_list(username):#白名单
						accept_article(id)
					elif is_in_recommend_list(username):
						print('is in recommend list')
						accept_article(id) 
						recommend_article(note_id)
					elif has_key_word_text(slug) != None:#文章内容有关键字
						print('has keyword')
						reject_article_with_reason(id,keyword)
					else:
						accept_article(id) 
		except Exception:
			print('审核完一轮')
			print('str(Exception):\t\t'),str(Exception)
		time.sleep(60*1) #单位second  30分钟
